import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
//import page disini

const index = () => {
  return (
    <View style={styles.container}>
      <Text>Hallo kelas react native lanjutan Sanbercode !</Text>
    </View>
  )
}

export default index

const styles = StyleSheet.create({
  container :{
    flex : 1,
    backgroundColor : 'white',
    justifyContent: 'center',
    alignItems: "center",
  }
})
