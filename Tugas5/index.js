import React,{useState,createContext} from 'react'
import {Text, View } from 'react-native'
import Navigation from './src/navigation'
import firebase from '@react-native-firebase/app'

export const RootContext = createContext();


var firebaseConfig = {
  apiKey: 'AIzaSyCLRsAKJBaYMF185a_o4qJS6eC-Lw2Yk-0',
  authDomain: 'training-app-8f35a.firebaseapp.com',
  databaseURL: 'https://training-app-8f35a.firebaseio.com',
  projectId: 'training-app-8f35a',
  storageBucket: 'training-app-8f35a.appspot.com',
  messagingSenderId: '276304921759',
  appId: '1:276304921759:web:106b0f02a64fd06bd30eaa',
};
// Inisialisasi firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const index = () => {
  const [modal, setModal] = useState(false);
  const [photo, setPhoto] = useState(null);
    return (
      <RootContext.Provider
        value={{
          modal,
          setModal,
          photo,
          setPhoto
        }}>
        <Navigation />
      </RootContext.Provider>
    );
}

export default index
