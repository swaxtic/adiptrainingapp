import React,{useEffect,useState} from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from '../pages/SplashScreen';
import {
    Intro,
    Login,
    Profile,
    Register,
} from '../pages'

const Stack = createStackNavigator();

const MainStack = () =>{
    return (
      <Stack.Navigator initialRouteName="Register">
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Intro"
          component={Intro}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
}

const AppStack = () => {
    const [isLoading, setIsLoading] = useState(true)
    useEffect(()=>{
        setTimeout(()=>{
            setIsLoading(!isLoading)
        },3000)
    },[])
    if(isLoading){
        return <SplashScreen/>
    }
    return (
        <NavigationContainer>
            <MainStack/>
        </NavigationContainer>
    )
}

export default AppStack
