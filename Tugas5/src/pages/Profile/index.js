import React, {useEffect, useState} from 'react';
import {StyleSheet, Image, Text, View, Dimensions} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth'
import {GoogleSignin, GoogleSigninButton} from '@react-native-community/google-signin';
import api from '../../api';
import { Button } from '../../components/atoms';

const Profile = ({navigation}) => {
const [userInfo, setUserInfo] = useState(null)

  useEffect(()=>{
      async function getToken(){
          try{
              const token = await Asyncstorage.getItem("token")
              return getVenue(token)
              console.log('Token -> ',token)
          }catch(err){
              console.log(err)
          }
      }
      getToken()
      getCurrentUser()
  },[])

  const getCurrentUser = async()=>{
    const userInfo = await GoogleSignin.signInSilently()
    console.log(userInfo)
    setUserInfo(userInfo)
  }

  const onLogOutPress = async()=>{
    try{
        await GoogleSignin.revokeAccess()
        await GoogleSignin.signOut()
        await Asyncstorage.removeItem("token")
        navigation.navigate('Login')
    }catch(err){
        console.log(err)
    }
  }

  const getVenue = (token)=>{
      Axios({
          method: 'GET',
          url : `${api}/venues`,
          headers : {'Authorization':'Bearer'+token},
          timeout : 20000
      }).then((res)=>{
          console.log(res)
      }).catch((err)=>{
          console.log(err)
      })
  }

  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View style={styles.avatar}>
          <Image
            style={{flex: 1, width: undefined, height: undefined}}
            resizeMode="contain"
            source={{uri: userInfo && userInfo.user && userInfo.user.photo}}
          />
        </View>
        <Text style={[styles.bigTxt, {marginTop: 5}]}>
          {userInfo && userInfo.user && userInfo.user.name}
        </Text>
      </View>
      <View style={styles.infoContainer}>
        <View style={styles.infoTextContainer}>
          <Text>Tanggal Lahir</Text>
          <Text>18 September 1998</Text>
        </View>
        <View style={styles.infoTextContainer}>
          <Text>Jenis Kelamin</Text>
          <Text>Laki - Laki</Text>
        </View>
        <View style={styles.infoTextContainer}>
          <Text>Hobi</Text>
          <Text>Ngoding</Text>
        </View>
        <View style={styles.infoTextContainer}>
          <Text>No Telp</Text>
          <Text>081348756429</Text>
        </View>
        <View style={styles.infoTextContainer}>
          <Text>Email</Text>
          <Text>{userInfo && userInfo.user && userInfo.user.email}</Text>
        </View>
        <Button
          onPress={() => onLogOutPress()}
          marginTop={20}
          title="LOGOUT"
          backgroundColor="#0089D6"
        />
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profileContainer: {
    backgroundColor: '#3EC6FF',
    width: windowWidth,
    height: '35%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: 80,
    height: 80,
    backgroundColor: 'grey',
    borderRadius: 70 / 2,
    overflow: 'hidden',
  },
  infoContainer: {
    borderRadius: 10,
    marginTop: '-8%',
    alignSelf: 'center',
    elevation: 5,
    height: '40%',
    width: '90%',
    backgroundColor: '#ffff',
    padding: 10,
  },
  bigTxt: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  infoTextContainer: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
