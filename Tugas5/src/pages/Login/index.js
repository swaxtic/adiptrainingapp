import React,{useEffect, useState} from 'react'
import { StyleSheet, Text,TextInput,Image ,View } from 'react-native'
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage'
import auth from '@react-native-firebase/auth'
import {GoogleSignin, GoogleSigninButton} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id'
import api from '../../api'
import {Button,Form} from '../../components/atoms'

const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription : 'Touch Sensor',
  sensorErrorDescription : 'Failed',
  cancelText : 'Cancel',
};

const Login = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('');

    const saveToken = async(token) =>{
        try{
            await Asyncstorage.setItem("token",token)
        }catch (err){
            console.log(err)
        }
    }

   useEffect(() => {
     configureGoogleSignIn()
   }, []) 

    const configureGoogleSignIn =()=>{
      GoogleSignin.configure({
        webClientId : '276304921759-m525jbqthgdncmo43eqpsgaslmnb17og.apps.googleusercontent.com',
        offlineAccess : false,
      })
    }

    const signInWithGoogle= async ()=>{
      try{
        const {idToken} = await GoogleSignin.signIn()
        console.log(idToken)
        const credential = auth.GoogleAuthProvider.credential(idToken);
        auth().signInWithCredential(credential)
        navigation.navigate('Profile');
      }catch(err){
        alert('Login via google Gagal')
        console.log(err)
      }
    }
    
    const signInWithFingerPrint = () =>{
      TouchID.authenticate('',config)
      .then(success =>{
        alert('Authentication Success')
        navigation.navigate('Profile')
      }).catch(error => {
        alert('Authentication Failed')
      })
    }

    const onLoginPress = () =>{
        let data ={
            email : email,
            password : password,
        }
        Axios({
          method: 'post',
          url: `${api}/login`,
          data,
          timeout : 20000
        })
          .then(function (response) {
            //console.log("Respon - >",response);
            saveToken(response.data.token)
            navigation.navigate('Profile')
          })
          .catch(function (error) {
            console.log('Error: ' + error);
            alert('Login gagal')
          });
        // Axios.post(`${api}/login`,data,{
        //     timeout: 20000
        // })
        // .then((res)=> {
        //     console.log("Res Login -> ",res)
        // })
        // .catch((err) =>{
        //     console.log('Err Login -> ', err);
        // })
    }
    return (
      <View style={styles.container}>
        <Image
          style={styles.images}
          source={require('../../assets/images/logo.png')}
        />
        <View style={styles.formContainer}>
          <Text>Username</Text>
          <Form
            onChangeText={(e) => setEmail(e)}
            value={email}
            placeholder="Username or Email"
          />
          <Text>Password</Text>
          <Form
            onChangeText={(e) => setPassword(e)}
            value={password}
            secure={true}
            placeholder="Password"
          />
          <Button
            onPress={() => onLoginPress()}
            marginTop={20}
            title="LOGIN"
            backgroundColor="#0089D6"
          />
          <View style={styles.lineContainer}>
            <View style={styles.line}></View>
            <Text>OR</Text>
            <View style={styles.line}></View>
          </View>
          <GoogleSigninButton
            onPress={() => signInWithGoogle()}
            style={{width: null, height: 50}}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
          />
          <Button
            onPress={() => signInWithFingerPrint()}
            marginTop={20}
            title="SignIn With Fingerprint"
            backgroundColor="#191970"
          />
        </View>
        <View style={styles.footer}>
          <Text>Belum mempunyai akun ?</Text>
          <Text style={{color: '#0089D6'}} onPress={() => alert('buat akun')}>
            {' '}
            Buat Akun
          </Text>
        </View>
      </View>
    );
}

export default Login

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  lineContainer: {
    marginVertical : 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems : "center"
  },
  line: {
    // borderBottomWidth : 1,
    width : '45%',
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignSelf: 'center',
  },
  images: {
    marginVertical: 50,
    height: 160,
    width: 160,
  },
  formContainer: {
    // backgroundColor : 'pink',
    width: '90%',
    height: null,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    top: 200,
    height: 30,
    width: '90%',
  },
});
