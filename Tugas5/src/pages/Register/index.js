import React, {useEffect,useContext, useState} from 'react';
import {StyleSheet, Image, Text, View, Dimensions, TouchableOpacity} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Axios from 'axios';
import storage from '@react-native-firebase/storage';

import {RootContext} from '../../../index';
import {Camera} from '../index'
import {Button,Form} from '../../components/atoms';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Register = ({navigation}) => {
  const state = useContext(RootContext);
  
  const onRegisterPress = (uri) => {
    if(uri){
      uploadImage(uri);
    }else{
      alert('Masukkan Gambar')
    }
  };
  const uploadImage = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`images/user/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert('Upload Success');
      })
      .catch((err) => {
        alert(err);
      });
  };
  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View style={styles.avatar}>
          <Image
            style={{flex: 1, width: undefined, height: undefined}}
            resizeMode="cover"
            source={state.photo === null ? require('../../assets/images/person.jpg') :{uri: state.photo.uri}}
          />
        </View>
        <TouchableOpacity
          onPress={() => state.setModal(true)}
          style={{marginTop: 5}}>
          <Text style={styles.bigTxt}>Change Picture</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.infoContainer}>
        <View style={styles.formTextContainer}>
          <Text>Nama</Text>
          <Form placeholder="Name" />
        </View>
        <View style={styles.formTextContainer}>
          <Text>Email</Text>
          <Form placeholder="Email" />
        </View>
        <View style={styles.formTextContainer}>
          <Text>Password</Text>
          <Form placeholder="Password" secure={true} />
        </View>
        <Button
          onPress={() => onRegisterPress(state.photo.uri)}
          marginTop={20}
          title="REGISTER"
          backgroundColor="#0089D6"
        />
        <Camera close={() => state.setModal(false)} />
      </View>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profileContainer: {
    backgroundColor: '#3EC6FF',
    width: windowWidth,
    height: '35%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: 70,
    height: 70,
    backgroundColor: 'grey',
    borderRadius: 70 / 2,
    overflow: 'hidden',
  },
  infoContainer: {
    borderRadius: 10,
    marginTop: '-8%',
    alignSelf: 'center',
    elevation: 5,
    height: null,
    width: '90%',
    backgroundColor: '#ffff',
    padding: 10,
  },
  bigTxt: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  formTextContainer: {
    marginTop: 15,
  },
});
