import Intro from './Intro'
import Login from './Login'
import SplashScreen from './SplashScreen'
import Profile from './Profile'
import Register from './Register'
import Camera from './Camera'

export{
    Intro,
    Login,
    SplashScreen,
    Profile,
    Register,
    Camera,
};