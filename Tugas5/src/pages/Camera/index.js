import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Modal,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {RootContext} from '../../../index';

import {ButtonCircle} from '../../components/atoms'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Camera = ({close}) => {
  const state = useContext(RootContext);
  const [type, setType] = useState('back');

  const toggleCamera=()=>{
      setType(type === 'back' ? 'front': 'back')
  }

  const takePict = async()=>{
    const options ={quality : 0.5 , base64 : true}
    if(camera){
      const data= await camera.takePictureAsync(options)
      state.setPhoto(data)
      state.setModal(false)
    }
  }

  return (
    <Modal onRequestClose={close} transparent={true} visible={state.modal}>
      <View style={styles.container}>
        <RNCamera
          style={{flex: 1}}
          type={type}
          ref={ref => {
            camera = ref;
          }}>
          <View style={styles.btnFlipContainer}>
            <ButtonCircle onPress={()=> toggleCamera()} backgroundColor='white' width={40} height={40} borderRadius={20}  name="camera-reverse-outline" />
          </View>
          <View style={styles.rounded}/>
          <View style={styles.square}/>
          <View style={styles.btnTakeContainer}>
            <ButtonCircle onPress={() => takePict()} backgroundColor='white' width={65} height={65} borderRadius={65/2} name="camera-outline" />
          </View>
        </RNCamera>
      </View>
    </Modal>
  );
};

export default Camera;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnFlipContainer: {
    top: 10,
    width: null,
    height: null,
    left: 5,
  },
  btnTakeContainer: {
    // position : "absolute",
    height: null,
    width: null,
    top: '2%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rounded: {
    marginTop : '10%',
    alignSelf: 'center',
    borderRadius: 80,
    backgroundColor: 'transparent',
    width: '60%',
    height: '40%',
    borderColor: 'white',
    borderWidth: 1,
  },
  square: {
    marginTop : '20%',
    alignSelf: 'center',
    backgroundColor: 'transparent',
    width: '60%',
    height: '25%',
    borderColor: 'white',
    borderWidth: 1,
  },
});
