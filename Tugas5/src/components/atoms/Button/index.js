import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const Button = ({title,onPress,backgroundColor,width,marginTop}) => {
    return (
        <TouchableOpacity style={{...styles.container,marginTop,width,backgroundColor }} onPress={onPress}>
            <Text style={{fontSize : 18, color : 'white'}}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    container:{
        borderRadius : 5,
        height : 45,
        width : 180,
        justifyContent : "center",
        alignItems : "center",
    }
})
