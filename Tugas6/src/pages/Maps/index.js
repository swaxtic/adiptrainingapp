import React, { useEffect, useState} from 'react'
import { StyleSheet,Platform, Text, View } from 'react-native'
import MapBoxGL from '@react-native-mapbox-gl/maps'
import MapboxGL from '@react-native-mapbox-gl/maps';
MapBoxGL.setAccessToken(
  'pk.eyJ1Ijoic3dheHRpYyIsImEiOiJjazR6eGNscDIwZmF6M2t0ZTZxajR3ajBrIn0.st8KPMC3SMFVmq1ydPGPKA',
);

const coordinates = [
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.898690],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.609180, -6.898013]
]

const Maps = () => {
const [permission, setPermission] = useState(false);
    useEffect(() => {
        const task = async () => {
          const per = await hasLocationPermission();
          setPermission(per);
        };
        task();
    },[])
async function hasLocationPermission() {
  if (
    Platform.OS === 'ios' ||
    (Platform.OS === 'android' && Platform.Version < 23)
  ) {
    return true;
  }
  const isGranted = await MapBoxGL.requestAndroidLocationPermissions();

  console.log('isGranted', isGranted);
  return isGranted;
}
    return (
      <View style={styles.container}>
        <MapBoxGL.MapView style={{flex: 1}}>
          <MapBoxGL.UserLocation visible={true} />
          <MapBoxGL.Camera followUserLocation={true} />
          {coordinates.map((item, index) => {
            return (
              <MapboxGL.PointAnnotation
                key={index}
                id="pointAnnotaion"
                coordinate={item}>
                <MapboxGL.Callout title={item.toString()} />
              </MapboxGL.PointAnnotation>
            );
          })}
          {/* <MapboxGL.PointAnnotation id="pointAnnotaion" coordinate={coordinates[1]}>
          </MapboxGL.PointAnnotation> */}
        </MapBoxGL.MapView>
      </View>
    );
}

export default Maps

const styles = StyleSheet.create({
    container :{
        flex : 1,
    }
})
