import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {Card} from '../../components/atoms'
import Icon from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native-gesture-handler';


const Home = ({navigation}) => {
    return (
      <View style={styles.container}>
        <View style={styles.card}>
          <View style={styles.headerContainer}>
            <Text style={styles.txt}>Kelas</Text>
          </View>
          <View style={styles.iconContainer}>
            <TouchableOpacity onPress={() => navigation.navigate('Detail') }>
              <Icon size={60} color="#fff" name="logo-react" />
              <Text style={{alignSelf: 'center'}}>React Native</Text>
            </TouchableOpacity>
            <View>
              <Icon size={60} color="#fff" name="logo-python" />
              <Text style={{alignSelf: 'center'}}>Data Science</Text>
            </View>
            <View>
              <Icon size={60} color="#fff" name="logo-react" />
              <Text style={{alignSelf: 'center'}}>React Js</Text>
            </View>
            <View>
              <Icon size={60} color="#fff" name="logo-laravel" />
              <Text style={{alignSelf: 'center'}}>Laravel</Text>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.headerContainer}>
            <Text style={styles.txt}>Kelas</Text>
          </View>
          <View style={styles.iconContainer}>
            <View>
              <Icon size={60} color="#fff" name="logo-react" />
              <Text style={{alignSelf: 'center'}}>Wordpress</Text>
            </View>
            <View>
              <Icon size={60} color="#fff" name="server-outline" />
              <Text style={{alignSelf: 'center'}}>Desain Grafis</Text>
            </View>
            <View>
              <Icon size={60} color="#fff" name="server-outline" />
              <Text style={{alignSelf: 'center'}}>Web Server</Text>
            </View>
            <View>
              <Icon size={60} color="#fff" name="image-outline" />
              <Text style={{alignSelf: 'center'}}>UI/UX</Text>
            </View>
          </View>
        </View>
        <View style={styles.cardSummary}>
          <View style={styles.headerContainer}>
            <Text style={styles.txt}>Summary</Text>
          </View>
          <View style={styles.item}>
            <View style={styles.headItem}>
              <Text style={styles.txt}>React Native</Text>
            </View>
            <View style={styles.info}>
              <Text style={styles.txt}>Today</Text>
              <Text style={styles.txt}> 20 Orang</Text>
            </View>
            <View style={styles.info}>
              <Text style={styles.txt}>Total</Text>
              <Text style={styles.txt}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.item}>
            <View style={styles.headItem}>
              <Text style={styles.txt}>Data Science</Text>
            </View>
            <View style={styles.info}>
              <Text style={styles.txt}>Today</Text>
              <Text style={styles.txt}> 30 Orang</Text>
            </View>
            <View style={styles.info}>
              <Text style={styles.txt}>Total</Text>
              <Text style={styles.txt}>100 Orang</Text>
            </View>
          </View>
          <View style={styles.item}>
            <View style={styles.headItem}>
              <Text style={styles.txt}>React JS</Text>
            </View>
            <View style={styles.info}>
              <Text style={styles.txt}>Today</Text>
              <Text style={styles.txt}> 66 Orang</Text>
            </View>
            <View style={styles.info}>
              <Text style={styles.txt}>Total</Text>
              <Text style={styles.txt}>100 Orang</Text>
            </View>
          </View>
        </View>
      </View>
    );
}

export default Home

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
  },
  card: {
    marginBottom: 14,
    height: null,
    width: '100%',
  },
  headerContainer: {
    height: 30,
    width: '100%',
    backgroundColor: '#088dc4',
    justifyContent: 'center',
  },
  iconContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#3EC6FF',
  },
  cardSummary: {
    height: null,
    width: '100%',
    backgroundColor: '#088dc4',
  },
  headItem: {
    height: 30,
    width: '100%',
    justifyContent: 'center',
    backgroundColor: '#3EC6FF',
  },info:{
    justifyContent : "space-between",
    flexDirection : "row",
    paddingHorizontal : 10,
  },
  txt :{
    color : '#fff',
    fontWeight : "bold",
  }
});
