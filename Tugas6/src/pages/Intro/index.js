import React from 'react';
import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';
import {Login} from '../index';
const slides = [
  {
    key: 1,
    title: 'Belajar Intensif',
    text:
      '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
    image: require('../../assets/images/working-time.png'),
  },
  {
    key: 2,
    title: 'Teknologi Populer',
    text: 'Menggunakan bahasa pemrograman populer',
    image: require('../../assets/images/research.png'),
  },
  {
    key: 3,
    title: 'From Zero to Hero',
    text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
    image: require('../../assets/images/venture.png'),
  },
  {
    key: 4,
    title: 'Training Gratis',
    text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
    image: require('../../assets/images/money-bag.png'),
  },
];
const Intro = ({navigation}) => {
  const renderSlide = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image style={styles.image} source={item.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };
  const onDone = () => {
    navigation.navigate('Login')
  };
  const renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };
  const renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <View style={{flex: 1}}>
        <AppIntroSlider
          data={slides}
          onDone={onDone}
          renderItem={renderSlide}
          renderDoneButton={renderDoneButton}
          renderNextButton={renderNextButton}
          keyExtractor={(item, index) => index.toString()}
          activeDotStyle={{backgroundColor: '#191970'}}
        />
      </View>
    </View>
  );
};

export default Intro;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 200,
    height: 200,
    marginVertical: 32,
  },
  text: {
    fontSize: 18,
    color: '#a4a4a6',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#191970',
    textAlign: 'center',
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#191970',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
