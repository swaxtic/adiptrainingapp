import React, {useEffect, useState} from 'react';
import {StyleSheet, Image, Text, View, Dimensions} from 'react-native';
import {Button} from '../../components/atoms'
import auth from '@react-native-firebase/auth';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Profile = ({navigation}) => {
  const logOut =() =>{
    auth()
      .signOut()
      .then(() =>
        navigation.reset({
          index: 1,
          routes: [{name: 'Auth'}],
        }),
      );
  }
  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View style={styles.avatar}>
          <Image
            style={{flex: 1, width: undefined, height: undefined}}
            resizeMode="contain"
          />
        </View>
        <Text style={[styles.bigTxt, {marginTop: 5}]}>Muhammad Adip</Text>
      </View>
      <View style={styles.infoContainer}>
        <View style={styles.infoTextContainer}>
          <Text>Tanggal Lahir</Text>
          <Text>18 September 1998</Text>
        </View>
        <View style={styles.infoTextContainer}>
          <Text>Jenis Kelamin</Text>
          <Text>Laki - Laki</Text>
        </View>
        <View style={styles.infoTextContainer}>
          <Text>Hobi</Text>
          <Text>Ngoding</Text>
        </View>
        <View style={styles.infoTextContainer}>
          <Text>No Telp</Text>
          <Text>081348756429</Text>
        </View>
        <View style={styles.infoTextContainer}>
          <Text>Email</Text>
          <Text>adipsandro@gmail.com</Text>
        </View>
        <Button
          onPress={() => logOut()}
          marginTop={20}
          title="LOGOUT"
          backgroundColor="#0089D6"
        />
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profileContainer: {
    backgroundColor: '#3EC6FF',
    width: windowWidth,
    height: '35%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    width: 80,
    height: 80,
    backgroundColor: 'grey',
    borderRadius: 70 / 2,
    overflow: 'hidden',
  },
  infoContainer: {
    borderRadius: 10,
    marginTop: '-8%',
    alignSelf: 'center',
    elevation: 5,
    height: '40%',
    width: '90%',
    backgroundColor: '#ffff',
    padding: 10,
  },
  bigTxt: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  infoTextContainer: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
