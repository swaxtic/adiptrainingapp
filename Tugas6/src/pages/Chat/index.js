import React, {useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'
import {GiftedChat} from 'react-native-gifted-chat'

const Chat = ({navigation}) => {

    const [message, setMessage] = useState([])
    const [user, setUser] = useState({})

    useEffect(()=> {
        const user =auth().currentUser
        setUser(user)
        getData()
        return()=>{
            const db = database().ref('messages')
            if(db){
                db.off() //ketika aplikasi tdk digunakan di off
            }
        }
    },[])

    const getData =() => {
        database().ref('messages').limitToLast(20).on('child_added',snapshot => {
            const value = snapshot.val()
            setMessage(previosMessages => GiftedChat.append(previosMessages,value))
        })
    }

    const onSend =  ((message = []) =>{
        for (let i = 0; i < message.length; i++) {
            database().ref('messages').push({
                _id : message[i]._id,
                createdAt : database.ServerValue.TIMESTAMP,
                text : message[i].text,
                user : message[i].user
            })
        }
    })

    return (
      <GiftedChat
        messages={message}
        onSend={(message) => onSend(message)}
        user={{
          _id: user.uid,
          name: user.email,
          avatar:
            'https://materiell.com/wp-content/uploads/2015/04/susan-full.png',
        }}
      />
    );
}

export default Chat

const styles = StyleSheet.create({
    container : {
        flex : 1,
    }
})
