import SplashScreen from './SplashScreen'
import Profile from './Profile'
import Home from './Home'
import Maps from './Maps'
import Detail from './Detail'
import Chat from './Chat'
import Login from './Login'
import Intro from './Intro'

export{
    SplashScreen,
    Profile,
    Home,
    Maps,
    Detail,
    Chat,
    Login,
    Intro,
};