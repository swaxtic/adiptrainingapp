import React from 'react'
import { StyleSheet,Image,StatusBar, Text, View } from 'react-native'

const SplashScreen = () => {
    return (
        <View style={styles.container}>
            <StatusBar barStyle={"dark-content"} backgroundColor='white'/>
        <View style={styles.logoContainer}>
            <Image style={styles.image} source={require('../../assets/images/logo.png')}/>
        </View>
        <View style={styles.textContainer}>
            <Text style={styles.textLogo}>Training App</Text>
        </View>
        </View>
    )
}

export default SplashScreen

const styles = StyleSheet.create({
    container :{
        flex : 1,
        alignItems : "center",
        justifyContent : "center",
        backgroundColor : '#ffff'
    },
    logoContainer :{
        padding :20,
        alignItems : 'center',
        justifyContent : "center",
    },image :{
        width : 200,
        height : 200,

    },textContainer :{
        alignItems : "center",
        justifyContent : "center",
    },
    textLogo:{
        fontSize : 24,
        color : '#0000',
        fontWeight : "bold",
    }
})
