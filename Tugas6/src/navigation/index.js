import React,{useEffect,useState} from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import auth from '@react-native-firebase/auth';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-community/async-storage';

import Icon from 'react-native-vector-icons/Ionicons'
import {
    SplashScreen,
    Profile,
    Home,
    Maps,
    Detail,
    Chat,
    Login,
    Intro,
} from '../pages'
import { set } from 'react-native-reanimated';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const tabStack = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#0089D6',
      }}>
      <Tab.Screen
        name="Home"
        options={{
          headerShown: false,
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <Icon name="home" color={color} size={size} />
          ),
        }}
        component={Home}
      />
      <Tab.Screen
        name="Maps"
        options={{
          headerShown: false,
          tabBarLabel: 'Maps',
          tabBarIcon: ({color, size}) => (
            <Icon name="location" color={color} size={size} />
          ),
        }}
        component={Maps}
      />
      <Tab.Screen
        name="Chat"
        options={{
          headerShown: false,
          tabBarLabel: 'Chat',
          tabBarIcon: ({color, size}) => (
            <Icon name="chatbox" color={color} size={size} />
          ),
        }}
        component={Chat}
      />
      <Tab.Screen
        name="Profile"
        options={{
          headerShown: false,
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, size}) => (
            <Icon name="person" color={color} size={size} />
          ),
        }}
        component={Profile}
      />
    </Tab.Navigator>
  );
  }  

const AuthStack = ({initial='Login'}) => {
  return (
    <Stack.Navigator initialRouteName={initial}>
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Main"
        component={MainStack}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
  
}

const MainStack = () =>{
    return (
      <Stack.Navigator initialRouteName="Tab">
        <Stack.Screen
          name="Auth"
          component={AuthStack}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Tab"
          component={tabStack}
          options={{headerShown: false}}
        />
        <Stack.Screen name="Detail" component={Detail} />
      </Stack.Navigator>
    );
}

const AppStack = ({navigation}) => {
  const [isSigned, setSigned] = useState(false)
    const [isLoading, setIsLoading] = useState(true)
    const [firstLaunch, setFirstLaunch] = useState(null)
    useEffect(()=>{
      if (auth().currentUser){
        setSigned(true)
      }
        AsyncStorage.getItem('alreadyLaunched').then((value) => {
          if (value == null) {
            AsyncStorage.setItem('alreadyLaunched', 'true'); // No need to wait for `setItem` to finish, although you might want to handle errors
            setFirstLaunch('true');
          } else {
            setFirstLaunch('false');
          }
        });
        setTimeout(()=>{
            setIsLoading(!isLoading)
        },3000)
    },[])
    if(isLoading){
        return <SplashScreen/>
    }else{
      if (firstLaunch === null) {
        return null; 
      }
      else if (firstLaunch != 'true'){ //check first launch
        return isSigned ? ( ///check login
          <>
            <NavigationContainer>
              <MainStack />
            </NavigationContainer>
          </>
        ) : (
          <>
            <NavigationContainer>
              <AuthStack initial="Login" />
            </NavigationContainer>
          </>
        );
      }else{ //first launch
        return (
          <NavigationContainer>
            <MainStack initial="Intro" />
          </NavigationContainer>
        );
      }
    }  
}

export default AppStack
