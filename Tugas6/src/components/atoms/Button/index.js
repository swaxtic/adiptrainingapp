import React from 'react'
import { StyleSheet,TouchableOpacity, Text, View } from 'react-native'

const Button = ({title,onPress,backgroundColor,width,marginTop}) => {
    return (
        <TouchableOpacity style={{...styles.container,marginTop,width,backgroundColor }} onPress={onPress}>
            <Text style={{fontSize : 18, color : 'white'}}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    container:{
        borderRadius : 5,
        height : 45,
        width : 180,
        justifyContent : "center",
        alignItems : "center",
    }
})
