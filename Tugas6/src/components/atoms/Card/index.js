import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const Card = ({header}) => {
    return (
      <View style={styles.card}>
        <View style={styles.headerContainer}>
          <Text>Kelas</Text>
        </View>
        <View style={styles.iconContainer}>
          <View>
            <Icon size={60} color="#fff" name="logo-react" />
            <Text style={{alignSelf: 'center'}}>React</Text>
          </View>
          <View>
            <Icon size={60} color="#fff" name="logo-python" />
            <Text style={{alignSelf: 'center'}}>Data Science</Text>
          </View>
          <View>
            <Icon size={60} color="#fff" name="logo-react" />
            <Text style={{alignSelf: 'center'}}>React Js</Text>
          </View>
          <View>
            <Icon size={60} color="#fff" name="logo-laravel" />
            <Text style={{alignSelf: 'center'}}>Laravel</Text>
          </View>
        </View>
      </View>
    );
}

export default Card

const styles = StyleSheet.create({
  card: {
    height: '30%',
    width: '100%',
  },
  headerContainer: {
    height: 30,
    width: '100%',
    backgroundColor: '#088dc4',
    justifyContent: 'center',
  },
  iconContainer :{
      justifyContent : "space-between",
      alignItems : "center",
      flexDirection : "row",
      backgroundColor : '#3EC6FF'
  }
});
