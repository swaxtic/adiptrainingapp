import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const SmallSvatar = ({txt}) => {
    return (
      <View style={styles.avatar}>
        <Text style={{fontWeight: 'bold'}}>{txt}</Text>
      </View>
    );
}

export default SmallSvatar

const styles = StyleSheet.create({
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: '#ebebeb',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
