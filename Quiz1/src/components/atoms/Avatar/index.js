import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Avatar = ({txt}) => {
  return (
    <View style={styles.avatar}>
      <Text style={{fontWeight: 'bold'}}>{txt}</Text>
    </View>
  );
};

export default Avatar;

const styles = StyleSheet.create({
  avatar: {
    height: 70,
    width: 70,
    borderRadius: 35,
    backgroundColor: '#ebebeb',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
