import React from 'react'
import { StyleSheet, Text, View,Dimensions, TouchableOpacity, } from 'react-native'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const FloatingButton = ({onPress}) => {
    return (
        <TouchableOpacity style={styles.floatingButton}
        onPress={onPress}
        >
            <Text style={{alignSelf: "center",color: 'white',fontSize: 32}} >+</Text>
        </TouchableOpacity>
    )
}

export default FloatingButton

const styles = StyleSheet.create({
  floatingButton: {
    elevation: 5,
    zIndex : 1,
    position: 'absolute',
    top: windowHeight - 100,
    left: windowWidth - 70,
    borderRadius: 40,
    width: 65,
    height: 65,
    backgroundColor: '#0089D6',
    justifyContent: 'center',
  },
});
