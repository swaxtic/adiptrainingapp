import React from 'react'
import { StyleSheet, Text,TouchableOpacity, View } from 'react-native'

const ButtonMore = ({morePress}) => {
    return <TouchableOpacity onPress={morePress} style={styles.moreButton}></TouchableOpacity>;
}

export default ButtonMore

const styles = StyleSheet.create({
  moreButton: {
    height: 50,
    width: 30,
    backgroundColor: 'grey',
  },
});
