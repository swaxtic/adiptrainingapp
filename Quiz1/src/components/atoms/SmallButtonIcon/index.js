import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

const SmallButtonIcon = ({hapus}) => {
    return (
        <TouchableOpacity onPress={hapus} style={styles.container}>
            <Icon style={{color: 'tomato'}} size={28} name="delete" />
        </TouchableOpacity>
    )
}

export default SmallButtonIcon

const styles = StyleSheet.create({
    container:{

    }
})
