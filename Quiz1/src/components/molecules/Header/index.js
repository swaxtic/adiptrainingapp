import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Header = () => {
    return (
      <View style={styles.header}>
        <Text style={{fontSize: 24,color : 'white'}}>Contact</Text>
      </View>
    );
}

export default Header

const styles = StyleSheet.create({
  header: {
    position : "absolute",
    width: '100%',
    height: 50,
    backgroundColor: '#0089D6',
    borderBottomWidth: 0.3,
    paddingLeft: 5,
    justifyContent: 'center',
  },
});
