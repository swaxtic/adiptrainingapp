import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import ButtonMore from '../../atoms/ButtonMore'
import ButtonIcon from '../../atoms/SmallButtonIcon'
import SmallAvatar from '../../atoms/SmallAvatar'

const ContactInfo = ({infoPress,txtAva,number,name,deletePress}) => {
    return (
      <TouchableOpacity onPress={infoPress} style={styles.list}>
        <SmallAvatar txt={txtAva}/>
        <View style={styles.infoTxt}>
          <Text style={styles.txt}>{name}</Text>
          <Text style={styles.txt}>{number}</Text>
        </View>
        <ButtonIcon hapus={deletePress}/>
      </TouchableOpacity>
    );
}

export default ContactInfo

const styles = StyleSheet.create({
  list: {
    height: 70,
    width: null,
    // backgroundColor: '#ff00ff',
    marginBottom : 5,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 0,
  },
  infoTxt: {
    height: '80%',
    width: '75%',
      // backgroundColor: 'orange',
    justifyContent: 'center',
  },
  txt: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#525255',
  },
});
