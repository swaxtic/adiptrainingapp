import React,{useContext} from 'react'
import {
  StyleSheet,
  Text,
  Modal,
  TouchableHighlight,TouchableOpacity,
  Dimensions,
  View,
  TextInput
} from 'react-native';
import {RootContext} from '../../../index';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const NewContact = ({visible,close}) => {
  const state = useContext(RootContext)
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={visible}
        >
        <View style={styles.container}>
          <View style={styles.modalView}>
            <View style={styles.headerModal}>
              <Text style={{...styles.textStyle, fontSize: 24}}>
                New Contact
              </Text>
              <TouchableOpacity style={{alignSelf: 'center'}} onPress={close}>
                <Text style={{...styles.textStyle, color: 'grey'}}>CANCEL</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.bigHeader} />
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Name"
                value={state.name}
                style={styles.input}
                onChangeText={(txt) => changeText(txt)}></TextInput>
              <TextInput
                keyboardType ='numeric'
                placeholder="Number"
                value={state.number}
                style={styles.input}
                onChangeText={(txt) => changeNumber(txt)}></TextInput>
              <TouchableOpacity
                style={{...styles.button, top: '45%'}}
                onPress={() => addContact()}>
                <Text style={{...styles.textStyle, alignSelf: 'center'}}>
                  Save
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
}

export default NewContact

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  bigHeader:{
    backgroundColor :'#0089D6',
    width : '100%',
    height : '50%'
  },
  headerModal: {
    paddingHorizontal: 5,
    backgroundColor: '#0089D6',
    height: '5%',
    width: windowWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalView: {
    //margin: 20,
    width: windowWidth,
    height: windowHeight,
    backgroundColor: 'white',
    borderRadius: 20,
    alignItems: 'center',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#0089D6',
    width: 100,
    height: 50,
    borderRadius: 5,
    justifyContent: 'center',
  },
  inputContainer: {
    borderRadius : 5,
    top : -100,
    height : '44%',
    width : '95%',
    backgroundColor : 'white',
    alignItems : "center",
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    elevation: 5,
  },
  input: {
    marginTop : 5,
    width: '90%',
    height: 45,
    //backgroundColor : 'blue'
    borderBottomWidth: 1,
  },
});
