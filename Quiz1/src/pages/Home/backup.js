import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Modal,
  TouchableHighlight,
  Animated,
  Text,
  View,
  Dimensions,
  FlatList,
  SafeAreaView,
} from 'react-native';
import Detail from '../Detail';
import Header from '../../components/molecules/Header';
import FloatingButton from '../../components/atoms/FloatingButton';
import ContactList from '../../components/molecules/ContactInfo';
import NewContact from '../NewContact';

import {RootContext} from '../../../index';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Home = () => {
  const scrollY = new Animated.Value(0);
  const diffClamp = Animated.diffClamp(scrollY, 0, 50);
  const translateY = diffClamp.interpolate({
    inputRange: [0, 50], //ketike scroll sampai 50 maka output dijaalankan
    outputRange: [0, -50], //header sembunyi sebanyak50
  });
  const [modal, setModal] = useState(false);
  const [modalDetail, setModalDetail] = useState(false);
  const listContact = useContext(RootContext);
  openModal = (index) => {
    openDetail(index);
    setModalDetail(true);
  };
  useEffect(() => {
    setModal(false);
  }, [listContact.contact]);
  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          transform: [{translateY: translateY}],
          elevation: 5,
          zIndex: 1,
          useNativeDriver: true,
        }}>
        <Header />
      </Animated.View>
      {listContact.contact.length ? (
        // <Animated.View
        //   style={{
        //     transform: [{translateY: translateY}],
        //   }}>
        <SafeAreaView
          style={{
            width: null,
            backgroundColor: 'white',
            height: null,
          }}>
          <FlatList
            data={listContact.contact}
            keyExtractor={(x, i) => i.toString()}
            renderItem={({item, index}) => (
              <ContactList
                name={item.name}
                number={item.number}
                infoPress={() => openModal(index)}
                deletePress={() => deleteItem(index)}
                txtAva={item.name
                  .match(/\b(\w)/g)
                  .join('')
                  .toUpperCase()}
              />
            )}
            onScroll={(e) => {
              //untuk mendapatkan nilai scroll
              scrollY.setValue(e.nativeEvent.contentOffset.y);
            }}
          />
        </SafeAreaView>
      ) : (
        // </Animated.View>
        <Text style={{marginVertical: 10, alignSelf: 'center'}}>
          Contact is Empty
        </Text>
      )}
      <NewContact close={() => setModal(false)} visible={modal} />
      <Detail close={() => setModalDetail(false)} visible={modalDetail} />
      <FloatingButton onPress={() => setModal(true)} />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
