import React ,{useState,useEffect,useContext,useRef}from 'react'
import { StyleSheet,Modal,TouchableHighlight, Animated, Text, View,Dimensions,FlatList, SafeAreaView } from 'react-native'
import Detail from '../Detail'
import Header from '../../components/molecules/Header'
import FloatingButton from '../../components/atoms/FloatingButton'
import ContactList from '../../components/molecules/ContactInfo'
import NewContact from '../NewContact'

import {RootContext} from '../../../index';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const Home = () => {
  const scrollA = useRef(new Animated.Value(0)).current
  const scrollY = new Animated.Value(0)
  const diffClamp = Animated.diffClamp(scrollA,0,50)
  const translateY = diffClamp.interpolate({
    inputRange: [0, 50], //ketike scroll sampai 50 maka output dijaalankan
    outputRange: [0, -50], //header sembunyi sebanyak50
  });
    const [modal, setModal] = useState(false);
    const [modalDetail, setModalDetail] = useState(false)
    const listContact = useContext(RootContext);
    openModal = (index) => {
      openDetail(index)
      setModalDetail(true)
    };
    useEffect(() => {
      setModal(false)
    }, [listContact.contact]);
    return (
      <View style={styles.container}>
          {/* <Header /> */}
          <Animated.View style={styles.header(diffClamp)}>
            <Text style={{fontSize: 24, color: 'white'}}>Contact</Text>
          </Animated.View>
        {listContact.contact.length ? (
        <Animated.View
          style={{...styles.list(diffClamp),alignSelf: "center",}}>
          <Animated.FlatList
              data={listContact.contact}
              keyExtractor={(x, i) => i.toString()}
              renderItem={({item, index}) => (
                <ContactList
                  name={item.name}
                  number={item.number}
                  infoPress={() => openModal(index)}
                  deletePress={() => deleteItem(index)}
                  txtAva={item.name
                    .match(/\b(\w)/g)
                    .join('')
                    .toUpperCase()}
                />
              )}
              onScroll={Animated.event(
                [{nativeEvent: {contentOffset: {y: scrollA}}}],
                {useNativeDriver : true}
              )}
              scrollEventThrottle={16}
            />
          </Animated.View>
        ) : (
          <Text style={{marginVertical: 10, alignSelf: 'center'}}>
            Contact is Empty
          </Text>
        )}
        <NewContact close={() => setModal(false)} visible={modal} />
        <Detail close={() => setModalDetail(false)} visible={modalDetail} />
        <FloatingButton onPress={() => setModal(true)} />
      </View>
    );
}

export default Home

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: (scrollA) => ({
    width: null,
    backgroundColor: 'white',
    height: null,
    transform: [
      {
        translateY: scrollA.interpolate({
          inputRange: [0, 65], //ketike scroll sampai 50 maka output dijaalankan
          outputRange: [0, -50], //header sembunyi sebanyak50
          extrapolate: 'clamp',
        }),
      },
    ],
    // transform: [
    //   {
    //     scale: scrollA.interpolate({
    //       inputRange: [0, 50], //ketike scroll sampai 50 maka output dijaalankan
    //       outputRange: [0,-50], //header sembunyi sebanyak50
    //     }),
    //   },
    // ],
  }),
  header: (scrollA) => ({
    // position: 'absolute',
    width: '100%',
    height: 50,
    transform: [
      {
        translateY: scrollA.interpolate({
          inputRange: [0, 53], //ketike scroll sampai 50 maka output dijaalankan
          outputRange: [0, -50], //header sembunyi sebanyak50
          extrapolate: 'clamp',
        }),
      },
      // {
      //   scale: scrollA.interpolate({
      //     inputRange: [0,50], //ketike scroll sampai 50 maka output dijaalankan
      //     outputRange: [0,-50], //header sembunyi sebanyak50
      //   }),
      // },
    ],
    backgroundColor: '#0089D6',
    borderBottomWidth: 0.3,
    paddingLeft: 5,
    justifyContent: 'center',
  }),
});
