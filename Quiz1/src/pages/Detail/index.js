import React,{useState,useEffect,useContext}from 'react'
import { StyleSheet,Modal, Text, TouchableOpacity, View,Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

import {RootContext} from '../../../index';

import Avatar from '../../components/atoms/Avatar'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Detail = ({close,visible}) => {
    const rootContext = useContext(RootContext);
    const anDetail = rootContext.contact[rootContext.detail];
    return (
      <Modal animationType="slide" transparent={true} visible={visible}>
        {rootContext.contact.length ? (
          <View style={styles.container}>
            <View style={styles.modalView}>
              <View style={styles.headerModal}>
                <TouchableOpacity style={{alignSelf: 'center'}} onPress={close}>
                  <Text style={{...styles.textStyle, color: 'grey'}}>
                    CLOSE
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.bigHeader}>
                <Avatar
                  txt={anDetail.name
                    .match(/\b(\w)/g)
                    .join('')
                    .toUpperCase()}
                />
                <Text style={{...styles.textStyle, marginTop: 5}}>
                  {anDetail.name}
                </Text>
                <View style={styles.topButtonContainer}>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => alert('Call')}>
                    <Icon style={{color: '#0089D6'}} size={28} name="call" />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => alert('Chat')}>
                    <Icon style={{color: '#0089D6'}} size={28} name="chat" />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => alert('VideoCall')}>
                    <Icon
                      style={{color: '#0089D6'}}
                      size={28}
                      name="videocam"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => alert('Email')}>
                    <Icon style={{color: '#0089D6'}} size={28} name="email" />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.info}>
                <Text>mobile</Text>
                <Text style={{fontSize: 18, color: '#0089D6'}}>
                  {anDetail.number}
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <View style={styles.container}>
            <Text>Masi Kosong</Text>
          </View>
        )}
      </Modal>
    );
}

export default Detail

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  modalView: {
    //margin: 20,
    width: windowWidth,
    height: windowHeight,
    backgroundColor: 'white',
    borderRadius: 20,
    alignItems: 'center',
  },
  bigHeader: {
    backgroundColor: '#0089D6',
    width: '100%',
    height: '25%',
    alignItems : 'center'
  },
  headerModal: {
    paddingHorizontal: 5,
    backgroundColor: '#0089D6',
    height: '5%',
    width: windowWidth,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  topButtonContainer :{
    flexDirection : "row",
    top : 22,
    width : '100%',
    height : 60,
    // backgroundColor : 'tomato',
    elevation : 1,
    justifyContent : "space-between",
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
  },
  info:{
    paddingVertical : 5,
    paddingLeft : 5,
    height : 60,
    width : '100%',
    backgroundColor : 'white',
    elevation : 1,
  },
  button: {
    // borderWidth : 1,
    backgroundColor: 'white',
    width: '24.5%',
    height: 50,
    alignItems : "center",
    justifyContent: 'center',
  },
});