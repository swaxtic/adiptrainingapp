import React, {useState, createContext} from 'react';
import { StyleSheet, Text, View } from 'react-native'
import Home from './src/pages/Home'

export const RootContext = createContext();

const index = () => {
    const [name, setName] = useState('');
    const [number, setNumber] = useState('');
    const [detail, setDetail] = useState(0);
    const [contact, setContact] = useState([
      {name: 'Muhammad Adip', number: '081348756429'},
      {name: 'John Doe', number: '0813********'},
      {name: 'Sewr o', number: '081348756429'},
      {name: 'Dummy Person 1', number: '0813********'},
      {name: 'Dummy Person 2', number: '081348756429'},
      {name: 'Dummy Person 3', number: '0813********'},
      {name: 'Dummy Person 4', number: '081348756429'},
      {name: 'Dummy Person 5', number: '0813********'},
      {name: 'Dummy Person 6', number: '081348756429'},
      {name: 'Dummy Person 7', number: '0813********'},
      {name: 'Dummy Person 8', number: '081348756429'},
      {name: 'Dummy Person 9', number: '0813********'},
      {name: 'Dummy Person 10', number: '081348756429'},
      {name: 'Dummy Person x', number: '0813********'},
      {name: 'Dummy Person x', number: '081348756429'},
      {name: 'Dummy Person x', number: '0813********'},
      {name: 'Dummy Person x', number: '081348756429'},
    ]);
    openDetail=(key)=>{
      setDetail(key)
    }
    deleteItem = (key) => {
      contact.splice(key, 1);
      setContact([...contact]);
    };
    changeText = (value) => {
      setName(value);
    };
    changeNumber = (value) => {
      setNumber(value);
    };
    addContact = () => {
      if (name && number) {
        setContact([
          ...contact,
          {
            name,
            number
          },
        ]);
        setName('');
        setNumber('')
        alert('Saved !')
      } else {
        alert('Please Enter Name and number');
      }
    };
    return (
      <RootContext.Provider
        value={{
          name,
          number,
          contact,
          detail,
          //  deleteItem,
          //  changeText,
          //  changeNumber,
          //addContact,
        }}>
        <Home />
      </RootContext.Provider>
    );
}

export default index

const styles = StyleSheet.create({})
