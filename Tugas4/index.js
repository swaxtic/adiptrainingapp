import React , {useState,createContext} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Todolist from './src/Pages/Todolist'
////TODOLIST DGN CONTEXT API

export const RootContext = createContext()

const index = () => {
    const [note,setNote] = useState('')
    const [todo, setTodo] = useState([])

    deleteItem = (key) => {
      todo.splice(key, 1);
      setTodo([...todo]);
    };
    changeText =(value) => {
     setNote(value)
    }
    addItem = () => {
      if (note) {
        const day = new Date().getDate()
        const month = new Date().getMonth();
        const year = new Date().getFullYear();
        const today = `${day}/${month}/${year}`
        // todo.push({
        //   date: today,
        //   title: note,
        // });
        setTodo([...todo,{
            date : today,
            title : note
        }]);
        setNote('');
      } else {
        alert('masukkan note');
      }
    };
    return (
      <RootContext.Provider value={{
          note,
          todo,
          deleteItem,
          addItem,
      }}>
        <Todolist />
      </RootContext.Provider>
    );
}

export default index

const styles = StyleSheet.create({})
