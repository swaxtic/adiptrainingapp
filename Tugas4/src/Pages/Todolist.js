import React, {useState, useEffect,useContext} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
  View,
  FlatList,
  ScrollView,
} from 'react-native';
import {RootContext} from '../../index'
import Note from '../Components/Note';

const Todolist = () => {
  const state = useContext(RootContext)

  // const [todo, setTodo] = useState([]);
  // const [note, setNote] = useState('');
  useEffect(() => {
    //console.log(state)
    //console.log(todo)
  }, [state.note]);

  return (
    <View style={styles.container}>
      <Text style={{marginVertical: 5}}>Masukan todo List</Text>
      <View style={styles.txtInput}>
        <TextInput
          placeholder="Input here"
          style={styles.input}
          onChangeText={(noteText) => changeText(noteText)}
          value={state.note}></TextInput>
        <TouchableOpacity onPress={() => addItem()} style={styles.buttonAdd}>
          <Text style={{fontSize: 24, color: 'black'}}>+</Text>
        </TouchableOpacity>
      </View>
      {state.todo.length ? (
        // state.todo.map((item, index) => {
        //   return (
        //     <Note
        //       key={index}
        //       date={item.date}
        //       title={item.title}
        //       onpres={() => state.deleteItem(index)}
        //     />
        //   );
        // })
        <View>
          <FlatList
            data={state.todo}
            keyExtractor={(x, i) => i.toString()}
            renderItem={({item,index}) => (
              <Note
                date={item.date}
                title={item.title}
                onpres={() => state.deleteItem(index)}
              />
            )}
          />
        </View>
      ) : (
        <Text style={{marginVertical: 10, alignSelf: 'center'}}>
          Todo list empty
        </Text>
      )}
    </View>
  );
};

export default Todolist;

const styles = StyleSheet.create({
  container: {
    paddingTop: 5,
    backgroundColor: 'white',
    flex: 1,
    paddingHorizontal: 5,
  },
  txtInput: {
    marginBottom: 10,
    height: '8%',
    width: null,
    //backgroundColor : 'pink',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  input: {
    width: '87%',
    height: 45,
    //backgroundColor : 'blue'
    borderWidth: 1,
  },
  buttonAdd: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 45,
    height: 45,
    backgroundColor: '#3EC6FF',
  },
});