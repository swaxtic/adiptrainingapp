import React, {useContext,useEffect,useState} from 'react'
import { StyleSheet,FlatList,TouchableOpacity, Text, View, Image } from 'react-native'
import auth from '@react-native-firebase/auth';
import {Header,ListJob} from '../../components/molecules'
import {RootContext} from '../../../index';
import jobapi from '../../api/jobapi';
import Axios from 'axios';
import {
  GoogleSignin,
} from '@react-native-community/google-signin';

const Home = ({navigation}) => {
 const state = useContext(RootContext);
 const [JobList , setJoblist] = useState([])
    useEffect(() => {
      GoogleSignin.configure({});
        async function getJob() {
          await Axios.get(`${jobapi}/positions.json?`, {
            timeout : 20000
          })
            .then((response) => {
              const data = response.data;
              setJoblist(data);
              //console.log(data)
              // setLoading(false);
            })
            .catch((error) => {
              console.log(error.response);
            });
        }
    getJob()
    getCurrentUser()
    }, []);

    const getCurrentUser = async () => {
      const userInfo = await GoogleSignin.signInSilently();
      //console.log(userInfo);
      state.setUserInfo(userInfo);
    };

    const applyButton =(company)=>{
      alert('Sending your document to '+company+' Success')
    }

    return (
      <View style={styles.container}>
        <Header
          profil={() => navigation.navigate('Profile')}
          name={
            state.userInfo === null
              ? 'Muhammad Adip'
              : state.userInfo &&
                state.userInfo.user &&
                state.userInfo.user.name
          }
          ava={
            state.userInfo === null
              ? require('../../assets/images/person.jpg')
              : {
                  uri:
                    state.userInfo &&
                    state.userInfo.user &&
                    state.userInfo.user.photo,
                }
          }
        />
        <View style={styles.listContainer}>
          <View style={{alignItems: "center",marginVertical: 10, flexDirection : "row",justifyContent : 'space-between'}}>
            <Text
              style={{
                ...styles.txt,
                fontWeight: 'bold',
                fontSize: 24,
              }}>
              Job For You
            </Text>
            <TouchableOpacity onPress={() => navigation.navigate('Stats')}>
              <Text style={{
                color : '#1089FF',
                fontWeight: 'bold',
                fontSize: 18,
              }}>View Stats</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={JobList.slice(0, 10)}
            //onEndReached={() => handleLoadMore(slice)}
            //onEndReachedThreshold={0.5}
            keyExtractor={(x, i) => i.toString()}
            renderItem={({item}) => (
              <ListJob
                detail={() => navigation.navigate('Detail', {jobId: item.id})}
                title={item.title}
                company={item.company}
                type={item.type}
                logo={item.company_logo}
                onPress={() => applyButton(item.company)}
              />
            )}
          />
        </View>
      </View>
    );
}

export default Home

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  listContainer :{
      paddingHorizontal : 14,
      backgroundColor : '#fff',
      width : null,
      height : '77%',
      top : -10,
      borderTopRightRadius : 10,
      borderTopLeftRadius : 10,
  },
  txt:{
      color : '#333'
  },
});
