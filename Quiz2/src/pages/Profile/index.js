import React, {useEffect,useContext, useState} from 'react';
import {StyleSheet, Image, Text,processColor, View, Dimensions, TouchableOpacity} from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import api from '../../api';
import Asyncstorage from '@react-native-community/async-storage';
import TouchID from 'react-native-touch-id';
import {RootContext} from '../../../index';
import {GoogleSignin, GoogleSigninButton} from '@react-native-community/google-signin';
import {PieChart} from 'react-native-charts-wrapper';
import {Camera} from '../index';
import { Button,Card } from '../../components/atoms';

const config = {
  title: 'Authentication Required',
  imageColor: '#1089FF',
  imageErrorColor: 'tomato',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

const Profile = ({navigation}) => {
const [isLoading, setIsLoading] = useState(false);
const state = useContext(RootContext);
const [uid , setUid] = useState('') 
// state.idCard.substring(state.idCard.lastIndexOf('/') + 1, state.length);

  useEffect(()=>{
    GoogleSignin.configure({});
    getFile()
    getCurrentUser()
  },[])

const getFile= async() => {
  setIsLoading(true);
  await storage()
    .ref(`images/user/${uid}`)
    .getDownloadURL()
    .then((r) => {
      // console.log(r)
      // return r
      state.setIdCard(r);
    })
    .catch((err) => {
      state.setIdCard(null);
    });
  setIsLoading(false);
}
  const getCurrentUser = async()=>{
    const userInfo = await GoogleSignin.signInSilently()
    // state.setIdCard(image)
    console.log(userInfo)
    state.setUserInfo(userInfo)
    setUid(auth().currentUser.uid);
  }
const connectGoogle = async () => {
  try {
    const {idToken} = await GoogleSignin.signIn();
    console.log(idToken);
    const credential = auth.GoogleAuthProvider.credential(idToken);
    auth().signInWithCredential(credential);
    getCurrentUser()
    alert('Connected')
  } catch (err) {
    alert('Login via google Gagal');
    console.log(err);
  }
};
  const onLogOutPress = async()=>{
    await auth().signOut();
    try{
      if(state.userInfo){
        await GoogleSignin.revokeAccess()
        await GoogleSignin.signOut()
      }else{
        await Asyncstorage.removeItem('token');
      }
        navigation.reset({
          index: 0,
          routes: [{name: 'Auth'}],
        });
    }catch(err){
        console.log(err)
    }
  }
  const saveUpload = async (uri) => {
    if(!uri){
      alert('please select image')
    }else{
    return storage()
      .ref(`images/user/${uid}`)
      .putFile(uri)
      .then((response) => {
        alert('Upload Success');
        state.setUploadIdCard(null)
        getFile();
      })
      .catch((err) => {
        alert(err);
      });
    }
  };
  const modalCamera = async () =>{
    if(!state.userInfo){
      alert('Please connect to google account to upload')
    }else{
      // state.setModal(true);
      TouchID.authenticate('', config)
        .then((success) => {
          alert('Authentication Success');
          state.setModal(true);
        })
        .catch((error) => {
          alert('Authentication Failed, Tolong letakkan sidik jari anda dengan benar');
        });
    }
  }
  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View>
          <Text style={[styles.bigTxt, {marginTop: 5}]}>
            {state.userInfo === null
              ? 'Muhammad Adip'
              : state.userInfo &&
                state.userInfo.user &&
                state.userInfo.user.name}
          </Text>
          <Text style={styles.txt}>Junior Frontend</Text>
          <View style={{marginTop: 20, flexDirection: 'row'}}>
            <Text style={{...styles.txt, fontSize: 10}}>Google Account </Text>
            {state.userInfo === null ? (
              <TouchableOpacity
                onPress={() => connectGoogle()}
                style={{borderRadius: 5, backgroundColor: '#D74D22'}}>
                <Text style={{...styles.txt, fontSize: 10}}>not connected</Text>
              </TouchableOpacity>
            ) : (
              <Text
                style={{
                  ...styles.txt,
                  fontSize: 10,
                  borderRadius: 5,
                  backgroundColor: '#0FD876',
                }}>
                connected
              </Text>
            )}
          </View>
        </View>
        <View style={styles.avatar}>
          <Image
            style={{flex: 1, width: undefined, height: undefined}}
            resizeMode="contain"
            source={
              state.userInfo === null
                ? require('../../assets/images/person.jpg')
                : {
                    uri:
                      state.userInfo &&
                      state.userInfo.user &&
                      state.userInfo.user.photo,
                  }
            }
          />
        </View>
      </View>
      {isLoading ? (
        <View style={{justifyContent: 'center'}}>
          <Text style={{alignSelf: 'center'}}>Loading ...</Text>
        </View>
      ) : (
        <View style={styles.infoContainer}>
          <View style={styles.rowInfo}>
            <Card
              onPress={() => modalCamera()}
              upload={state.uploadIdCard ? 'Ktp.jpg ' : null}
              file={state.idCard}
              txt="KTP"
              backgroundColor="#FDAC05"
            />
            <Card txt="CV" backgroundColor="#1089FF" />
          </View>
          <View style={{width: '100%', height: '40%', flexDirection: 'row'}}>
            <Card txt="FOTO" backgroundColor="#0FD876" />
          </View>
        </View>
      )}
      <View style={{width: '90%', alignSelf: 'center'}}>
        <Button
          onPress={() => saveUpload(state.uploadIdCard)}
          title="SAVE"
          backgroundColor="#1089FF"
        />
      </View>
      <View style={{width: '90%', alignSelf: 'center'}}>
        <Button
          onPress={() => onLogOutPress()}
          marginTop={10}
          title="LOGOUT"
          backgroundColor="#D74D22"
        />
      </View>
      <Camera close={() => state.setModal(false)} />
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profileContainer: {
    padding: 20,
    borderRadius: 20,
    marginVertical: '3%',
    backgroundColor: '#1089FF',
    alignSelf: 'center',
    width: '90%',
    height: '25%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  avatar: {
    width: 80,
    height: 80,
    backgroundColor: 'grey',
    borderRadius: 70 / 2,
    overflow: 'hidden',
  },
  infoContainer: {
    borderRadius: 10,
    marginTop: '1%',
    alignSelf: 'center',
    height: '55%',
    width: '90%',
    padding: 10,
  },
  bigTxt: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
  infoTextContainer: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rowInfo: {
    alignSelf: 'center',
    width: '100%',
    height: '40%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: '5%',
  },
  txt: {
    color: '#fff',
  },
});
