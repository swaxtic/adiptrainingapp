import React,{useEffect,useState} from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import jobapi from '../../api/jobapi';
import Axios from 'axios';
import MapBoxGL from '@react-native-mapbox-gl/maps';
MapBoxGL.setAccessToken(
  'pk.eyJ1Ijoic3dheHRpYyIsImEiOiJjazR6eGNscDIwZmF6M2t0ZTZxajR3ajBrIn0.st8KPMC3SMFVmq1ydPGPKA',
);

import {HeaderBigPicture} from '../../components/molecules'
const coordinates = [
  [107.598827, -6.896191],
];
const Detail = ({route,navigation}) => {
    const { jobId} = route.params;
    const [detailJob, setDetailJob] = useState([]);
    useEffect(() => {
      async function getJob() {
        await Axios.get(`${jobapi}/positions/${jobId}.json?`, {
          timeout: 20000,
        })
          .then((response) => {
            const data = response.data;
            //console.log(data)
            setDetailJob(data);
            //console.log(data)
            // setLoading(false);
          })
          .catch((error) => {
            console.log(error.response);
          });
      }
      getJob();
    }, []);
    return (
      <View style={styles.container}>
        <HeaderBigPicture poster={detailJob.company_logo} />
        <Text
          style={{
            ...styles.txt,
            marginHorizontal: 10,
            marginTop: 10,
            fontSize: 18,
            fontWeight: 'bold',
          }}>
          {detailJob.company}
        </Text>
        <ScrollView>
          <Text
            style={{
              ...styles.txt,
              marginHorizontal: 10,
              marginTop: 10,
              fontSize: 18,
              fontWeight: 'bold',
            }}>
            Location
          </Text>
          <MapBoxGL.MapView
            style={{height: 200, alignSelf: 'center', width: '95%'}}>
            <MapBoxGL.PointAnnotation
              id="pointAnnotaion"
              coordinate={coordinates[0]}>
              <MapBoxGL.Callout title={detailJob.company} />
            </MapBoxGL.PointAnnotation>
          </MapBoxGL.MapView>
          <Text
            style={{
              ...styles.txt,
              marginHorizontal: 10,
              marginTop: 10,
              fontSize: 18,
              fontWeight: 'bold',
            }}>
            Job Description
          </Text>

          <Text
            style={{
              ...styles.txt,
              marginHorizontal: 10,
              marginTop: 10,
              fontSize: 14,
            }}>
            {detailJob.description}
          </Text>
        </ScrollView>
      </View>
    );
}

export default Detail

const styles = StyleSheet.create({
    container :{
        flex : 1,
        backgroundColor : '#fff'
    },txt :{
        color : '#333',
        textAlign : "justify",
    }
})
