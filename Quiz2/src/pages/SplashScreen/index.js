import React from 'react'
import { StyleSheet,Image,StatusBar, Text, View } from 'react-native'

const SplashScreen = () => {
    return (
        <View style={styles.container}>
          <View style={styles.image}>
            <Text style={styles.txt}>Virtual JobFair</Text>
        </View>
        </View>
    )
}

export default SplashScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#1089FF',
  },
  image: {
    width: '100%',
    height: 102,
    resizeMode: 'cover',
    alignSelf : "center",
  },
  txt:{
    alignSelf : "center",
    fontWeight : "bold",
    fontSize : 32,
    color : '#fff',
  }
});
