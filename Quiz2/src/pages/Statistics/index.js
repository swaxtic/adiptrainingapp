import React,{useEffect,useState} from 'react'
import { StyleSheet, Text, processColor, View } from 'react-native'
import {PieChart} from 'react-native-charts-wrapper';
const Statistics = () => {
  const [data, setData] = useState([
    {value: 8,label :"Periklanan / Publikasi"},
    {value: 16, label :"Administrasi" },
    {value: 13, label :"Customer Service" },
    {value: 19, label :"Banking" },
    {value: 5, label :"Quality Control" },
    {value: 30, label : "Software Engineer" },
    {value: 7, label : "Manajemen"},
    {value: 14, label : "Designer / Grafis"},
    {value: 14, label : "Engineering"},
    {value: 20, label : "Accounting"},
  ]);
  const [color, setColor] = useState([
    '#FFD700',
    '#FF6347',
    '#DB7093',
    '#9370DB',
    '#C71585',
    '#BA55D3',
    '#DC143C',
    '#FFA07A',
    '#6A5ACD',
    '#CD5C5C',
    '#DA70D6',
    '#FFFFE0',
    '#F0E68C',
    '#DDA0DD',
    '#FFDAB9',
    '#E6E6FA',
    '#D8BFD8',
    '#FFE4B5',
  ]);
  const generateValue =() => {
    return data
  }
  const getConfigColors =() => {
    return color
  }
    const [chart, setChart] = useState({
      data: {
        dataSets: [
          {
            values: generateValue(),
            label: '',

            config: {
              colors:
                data.length > 0
                  ? getConfigColors().map((val) => processColor(val))
                  : [processColor('#fff')],
              valueTextSize: 14,
              valueTextColor: processColor('#1089FF'),
              sliceSpace: 1,
              selectionShift: 15,
              valueFormatter: "#.##'%'",
              valueLineColor: processColor('grey'),
              valueLinePart1Length: 0.2,
              valueLinePart2Length: 0.2,
              valueLineWidth: 1,
              valueLinePart1OffsetPercentage: 50,
              valueLineVariableLength: false,
              drawValues: true,
              drawEntryLabels: false,
              xValuePosition: 'OUTSIDE_SLICE',
              yValuePosition: 'OUTSIDE_SLICE',
              visible: true,
            },
          },
        ],
      },
    });
    const descStyle = {
      text: '',
      textSize: 15,
      textColor: processColor('darkgray'),
    };
    return (
      <View style={styles.container}>
        <PieChart
          touchEnabled={false}
          transparentCircleRadius={0}
          drawEntryLabels={false}
          entryLabelColor={processColor('white')}
          entryLabelTextSize={9}
          drawEntryLabels={true}
          holeRadius={60}
          usePercentValues={true}
          style={styles.pie}
          data={chart.data}
          chartDescription={descStyle}
          legend={{enabled: false}}
          highlights={[]}
        />
        <View
          style={{
            height: '30%',
            width: '100%',
          }}>
          <Text style={{...styles.txt,marginBottom : 10,fontSize : 18,fontWeight : "bold"}}>Statistic Bidang Pekerjaan</Text>
          {data.map((hasil,index)=> {
            return (
              <View key={index} style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text>{hasil.label}</Text>
                <View
                  style={{
                    marginHorizontal: 5,
                    height: 10,
                    width: 10,
                    backgroundColor: color[index],
                  }}></View>
                  <Text>Sebanyak : {hasil.value}</Text>
              </View>
            );
          })}
        </View>
      </View>
    );
}

export default Statistics

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 10,
  },
  pie: {
    alignSelf: 'center',
    width: '48%',
    height: '40%',
  },
  txt: {
    color: '#1089FF',
  },
});
