import Intro from './Intro'
import Login from './Login'
import SplashScreen from './SplashScreen'
import Profile from './Profile'
import Register from './Register'
import Camera from './Camera'
import Detail from './Detail'
import Home from './Home'
import Statistics from './Statistics'

export{
    Intro,
    Login,
    SplashScreen,
    Profile,
    Register,
    Camera,
    Detail,
    Home,
    Statistics,
};