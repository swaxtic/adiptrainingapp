import React from 'react'
import { Image, StatusBar, StyleSheet, Text, View } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';
const slides = [
  {
    key: 1,
    title: 'Find Your Dream Job',
    text: 'Cari dan temukan pekerjaan impianmu dengan mudah',
    image: require('../../assets/images/findJob.jpg'),
    backgroundColor: '#9B97F8',
  },
  {
    key: 2,
    title: 'Easily Manage Your CV',
    text: 'Kemudahan mengelola dokumen Lamaran',
    image: require('../../assets/images/document.jpg'),
    backgroundColor: '#017EC3',
  },
  {
    key: 3,
    title: 'All is Online',
    text: 'Semua proses dilakukan secara Online',
    image: require('../../assets/images/online.jpg'),
    backgroundColor: '#76B9C9',
  },
];
const Intro = ({navigation}) => {
    const renderSlide = ({item}) => {
        return(
            <View style={{...styles.slide,backgroundColor : item.backgroundColor}}>
                <Text style={styles.title}>{item.title}</Text>
                <Image style={styles.image} source={item.image}/>
                <Text style={styles.text}>{item.text}</Text>
            </View>
        )
    }
    const onDone = () =>{
        navigation.navigate('Login')
    }
    const renderDoneButton =()=>{
        return (
          <View style={styles.buttonCircle}>
            <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24}/>
          </View>
        );
    }
    const renderNextButton=()=>{
        return (
          <View style={styles.buttonCircle}>
            <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
          </View>
        );
    }
    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor="transparent" />
        <AppIntroSlider
          data={slides}
          onDone={onDone}
          renderItem={renderSlide}
          renderDoneButton={renderDoneButton}
          renderNextButton={renderNextButton}
          keyExtractor={(item, index) => index.toString()}
          activeDotStyle={{backgroundColor: '#1089FF'}}
        />
      </View>
    );
}

export default Intro

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  image: {
    width: '90%',
    height: 200,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  text: {
    bottom: '10%',
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  title: {
    marginVertical: '5%',
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#1089FF',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
