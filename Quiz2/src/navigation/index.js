import React,{useEffect,useState,useContext} from 'react'
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth'
import {RootContext} from '../../index';
import SplashScreen from '../pages/SplashScreen';
import {
    Intro,
    Login,
    Profile,
    Register,
    Home,
    Detail,
    Statistics,
} from '../pages'

const Stack = createStackNavigator();

const AuthStack = ({initial}) =>{
  return (
    <Stack.Navigator initialRouteName={initial}>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Intro"
        component={Intro}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Main"
        component={MainStack}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}
const MainStack = () =>{
    return (
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Stats"
          component={Statistics}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Detail"
          component={Detail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Auth"
          component={AuthStack}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
}

const AppStack = () => {
  const [firstLaunch, setFirstLaunch] = useState(null);
  const [user, setUser] = useState(auth().currentUser)
    const state = useContext(RootContext);
    const [isLoading, setIsLoading] = useState(true)
    useEffect(()=>{
      AsyncStorage.getItem('alreadyLaunched').then((value) => {
        if (value == null) {
          AsyncStorage.setItem('alreadyLaunched', 'true'); // No need to wait for `setItem` to finish, although you might want to handle errors
          setFirstLaunch('true');
        } else {
          setFirstLaunch('false');
        }
      });
        setTimeout(()=>{
            setIsLoading(!isLoading)
        },3000)
    },[])
    if(isLoading){
        return <SplashScreen/>
    }else{
      if(firstLaunch==='true' || !firstLaunch){
        return (
          <NavigationContainer>
            <AuthStack initial="Intro" />
          </NavigationContainer>
        );
      }else{
        return (
          <NavigationContainer>
            {user ? (
              <>
                <MainStack />
              </>
            ) : (
              <AuthStack initial='Login' />
            )}
          </NavigationContainer>
        );
      } 
    }
    // return (
    //   <NavigationContainer>
    //     {state.userInfo ? (
    //       <>
    //         <MainStack/>
    //       </>
    //     ) : (
    //       <AuthStack/>
    //     )}
    //   </NavigationContainer>
    // );
}

export default AppStack
