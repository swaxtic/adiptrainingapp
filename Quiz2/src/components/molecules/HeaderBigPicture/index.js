import React from 'react';
import {StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import {Button} from '../../atoms';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const HeaderBigPicture = ({poster}) => {
  return (
    <View style={styles.container}>
      <Image
        source={{
          uri: poster,
        }}
        style={{resizeMode: 'contain', flex: 1,marginTop : 20,}}
      />
      <View style={{left : '70%', bottom : '5%',borderRadius : 50,overflow : "hidden"}}>
        <Button backgroundColor='#1089FF' width={'25%'}title="Apply Now" />
      </View>
    </View>
  );
};

export default HeaderBigPicture;

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    backgroundColor: 'transparent',
    borderWidth : 0.3,
    height: '25%',
    width: '100%',
  },
});
