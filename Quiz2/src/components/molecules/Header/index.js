import React from 'react'
import { StyleSheet,TouchableOpacity, Image,Text, View } from 'react-native'

const Header = ({name,ava,profil}) => {
    return (
      <View style={styles.header}>
        <View style={styles.welcoming}>
          <Text style={{...styles.txt, fontWeight: 'bold', fontSize: 30}}>
            {name}
          </Text>
          <Text style={{...styles.txt, fontSize: 18}}>
            Welcome Back, Goodluck
          </Text>
        </View>
        <View>
          <View style={styles.ava}>
            <Image
              style={{flex: 1, width: undefined, height: undefined}}
              resizeMode="contain"
              source={ava}
            />
          </View>
          <TouchableOpacity onPress={profil} style={{marginTop: 5}}>
            <Text style={{...styles.txt, fontWeight: 'bold'}}>
              Visit Profile
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
}

export default Header

const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
    paddingHorizontal: 13,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#1089FF',
    height: '25%',
    width: null,
  },
  welcoming: {
    justifyContent: 'center',
    // backgroundColor: 'tomato',
    height: '60%',
    width: '70%',
  },
  ava: {
    width: 70,
    height: 70,
    borderRadius: 70 / 2,
    backgroundColor: 'grey',
    overflow: 'hidden',
  },
  txt: {
    color: '#fff',
  },
});
