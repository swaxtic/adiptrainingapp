import React from 'react'
import { StyleSheet,Image,TouchableOpacity, Text, View } from 'react-native'

const ListJob = ({detail,logo,title,type,company,onPress}) => {
    return (
      <TouchableOpacity onPress={detail} style={styles.job}>
        <Image source={{uri: logo}} style={styles.compLogo} />
        <View style={{justifyContent: 'space-between', width: '60%'}}>
          <Text style={{...styles.txt, fontSize: 18, fontWeight: 'bold'}}>
            {title}
          </Text>
          <Text>{company}</Text>
          <Text>{type}</Text>
        </View>
        <TouchableOpacity onPress={onPress} style={styles.infoLeft}>
          <Text style={{...styles.txt, fontWeight: 'bold', color: '#1089FF'}}>
            Apply Now
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
}

export default ListJob

const styles = StyleSheet.create({
  infoLeft: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    // backgroundColor : 'tomato',
    height: '30%',
    width: '20%',
  },
  job: {
    paddingHorizontal: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15,
    borderWidth: 0.5,
    borderRadius: 10,
    height: 100,
    width: null,
  },
  compLogo: {
    resizeMode: 'contain',
    height: '80%',
    width: '20%',
    //   backgroundColor : 'tomato'
  },
});
