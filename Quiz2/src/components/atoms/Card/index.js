import React from 'react'
import { StyleSheet,TouchableOpacity, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const Card = ({backgroundColor,upload,file,txt,onPress}) => {
  const ktp = 'ktp.jpg'
    return (
      <View style={{...styles.Card, backgroundColor, borderRadius: 20}}>
        <Text
          style={{
            ...styles.txt,
            fontSize: 32,
            fontWeight: 'bold',
            marginTop: '15%',
            alignSelf: 'center',
          }}>
          {txt}
        </Text>
        {file === null ? (
          <View style={styles.infoCard}>
            <TouchableOpacity onPress={onPress}>
              <Text style={{fontSize : 10,fontWeight: 'bold', color: '#D74D22'}}>
                {upload}(Perlu Upload)
              </Text>
            </TouchableOpacity>
            <Icon style={{color: '#D74D22'}} size={32} name="close-circle" />
          </View>
        ) : (
          <View style={styles.infoCard}>
            <TouchableOpacity onPress={onPress}>
              <Text style={{fontSize: 10,fontWeight: 'bold', color: '#1089FF'}}>{file}(uploaded)</Text>
            </TouchableOpacity>
            <Icon
              style={{color: '#1089FF'}}
              size={32}
              name="checkmark-circle"
            />
          </View>
        )}
      </View>
    );
}

export default Card

const styles = StyleSheet.create({
    Card :{
        elevation : 5,
        width : '48%',
        height :'100%',
        justifyContent : "space-between",
    },txt:{
        color :'#fff'
    },infoCard :{
        justifyContent : "space-between",
        flexDirection : "row",
        alignItems : "center",
        padding : 5,
        height : '35%',
        width : null,
        backgroundColor : '#fff',
        borderBottomRightRadius : 20,
        borderBottomLeftRadius : 20,
    }
})
