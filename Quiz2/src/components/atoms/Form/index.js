import React from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';

const Form = ({
  onChangeText,
  value,
  height,
  width,
  keyboardType = 'default',
  marginHorizontal,
  placeholder,
  secure = false,
  marginTop,
  alignSelf,
}) => {
  return (
    <TextInput
      style={[
        {
          marginHorizontal: marginHorizontal,
          height: height,
          width: width,
          marginTop: marginTop,
          alignSelf: alignSelf,
        },
        styles.container,
      ]}
      value={value}
      onChangeText={onChangeText}
      placeholder={placeholder}
      keyboardType={keyboardType}
      secureTextEntry={secure}></TextInput>
  );
};

export default Form;

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: '#1089FF',
    backgroundColor: '#F9F9F9',
  },
});
