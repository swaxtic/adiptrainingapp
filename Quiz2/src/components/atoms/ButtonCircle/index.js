import React from 'react';
import {StyleSheet,TouchableOpacity ,Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const Button = ({name, onPress, backgroundColor,height, borderRadius, width, marginTop}) => {
  return (
    <TouchableOpacity
      style={{...styles.container, marginTop,height,borderRadius, width, backgroundColor}}
      onPress={onPress}>
      <Icon name={name} size={24} />
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    borderRadius: 45/2,
    height: 45,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
