/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './Tugas1';
import Profile from './Tugas2';
import Tugas3 from './Tugas3';
import Tugas4 from './Tugas4';
import Quiz1 from './Quiz1' //Contact APP
import Quiz2 from './Quiz2' //Jobfair APP
import Tugas5 from './Tugas5' //Tugas SplashScreen // Tugas JWT //Tugas Google Sign IN //RN Upload Images
import Tugas6 from './Tugas6' //homepage //chart //realtime data management (Chat) // Complex navigation
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Quiz2);
