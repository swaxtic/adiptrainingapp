import React, {useState, useEffect} from 'react';
import { StyleSheet,TouchableOpacity, Text,TextInput, View, ScrollView } from 'react-native'
import Note from '../components/Note'

const Main = () => {
    const [todo, setTodo] = useState([]);
    const [note, setNote] = useState('');

    useEffect(() => {
        // console.log(note)
        // console.log(todo)
    }, [note]);
  
var deleteItem = (key) =>{
    todo.splice(key, 1);
    setTodo([...todo]);
}
  var addItem = () => {
    if (note) {
      var d = new Date();
      todo.push({
        date: d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate(),
        title: note,
      });
      setTodo([
        ...todo, 
    ]);
      setNote('')
    }else{
        alert('masukkan note')
    }
  };
    return (
      <View style={styles.container}>
        <Text style={{marginVertical: 5}}>Masukan todo List</Text>
        <View style={styles.txtInput}>
          <TextInput
            placeholder="Input here"
            style={styles.input}
            onChangeText={(noteText) => setNote(noteText)}
            value={note}></TextInput>
          <TouchableOpacity onPress={() => addItem()} style={styles.buttonAdd}>
            <Text style={{fontSize: 24, color: 'black'}}>+</Text>
          </TouchableOpacity>
        </View>
          {todo.length ? (
            todo.map((item, index) => {
              return (
                <Note
                  key={index}
                  date={item.date}
                  title={item.title}
                  onpres={() => deleteItem(index)}
                />
              );
            })
          ) : (
            <Text style={{marginVertical: 10, alignSelf: 'center'}}>
              Todo list empty
            </Text>
          )}
      </View>
    );
}

export default Main

const styles = StyleSheet.create({
  container: {
    paddingTop: 5,
    backgroundColor: 'white',
    flex: 1,
    paddingHorizontal: 5,
  },
  txtInput: {
    marginBottom : 10,
    height: '8%',
    width: null,
    //backgroundColor : 'pink',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  input: {
    width: '87%',
    height: 45,
    //backgroundColor : 'blue'
    borderWidth: 1,
  },
  buttonAdd: {
    alignItems : "center",
    justifyContent : "center",
    width: 45,
    height: 45,
    backgroundColor: '#3EC6FF',
  },
});
