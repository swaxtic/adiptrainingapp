import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

const Note = ({onpres,title,date}) => {
    return (
        <View style={styles.note}>
            <View style={styles.list}>
                <Text>{date}</Text>
                <Text>{title}</Text>
            </View>
            <TouchableOpacity onPress={onpres} >
                <Icon style={{color: 'tomato'}} size={28} name='delete'/>
            </TouchableOpacity>
        </View>
    )
}

export default Note

const styles = StyleSheet.create({
    note :{
        //backgroundColor: 'pink',
        borderWidth : 3,
        borderColor: 'grey',
        height : '10%',
        width : null,
        paddingHorizontal: 5,
        flexDirection: "row",
        justifyContent :'space-between',
        alignItems : "center",
        marginBottom : 10,
    },
    list :{
        paddingHorizontal : 5,
    }
})
